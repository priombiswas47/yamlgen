Setup visual studio development environment
================================================

## Required installations

   1. Visual Studio 2013 or later
   2. .NET Framework 4.5 or later
   3. Wix toolset version 3.11 (for setup project) [Download Wix Toolset](https://wixtoolset.org/releases/)\
     <span style="color:red">**Note:**</span> For Wix toolset, please download and install both main exe and visual studio extension from the above link
   4. Enterprise Architect version 13.5 or later   
   
## Steps (Build and run main project)

1. Clone the project https://erbenschell.iese.fraunhofer.de/FERAL/app/bcnga/camv.git
2. There are 2 projects:
	- Main project
	- Setup project
3. Source code of **main project** and **setup project** can be found in below directories respectively: 
   - *Modules\de.fraunhofer.iese.feral.app.camv.eaYamlExportPlugin*
   - *Modules\de.fraunhofer.iese.feral.app.camv.eaYamlExportPluginInstaller*
4. After installation of Enterprise Architect, check the registry editory \
    ***Computer\HKEY_CURRENT_USER\SOFTWARE\Sparx Systems\*** \
   If <span style="color:red">**EAAddins**</span>  key is not found, add the key. Under the key, add another 
   key named <span style="color:red">**YamlGenerator**</span> and 
   set the default value to <span style="color:red">**YamlGenerator.YamlGeneratorMain**</span>. 
   This step is necessary to display the plugin in enterprise architect 
   plugin menu and also for build purposes in visual studio.
     ![Registry](./img/Registry 1.png) 
5. In visual studio, select **YamlGenerator** project, right click and select properties. 
   - From the **Application** tab, select **Assembly Information** and check <span style="color:red">**Make assembly com visible**</span> checkbox.
   ![Assembly information](./img/Property 2.png) 
   - From the **Build** tab, check <span style="color:red">**Register for com interop**</span> checkbox.
   ![Com interop](./img/Property 3.png) 
6. Remove the <span style="color:red">**YamlDotNet**</span> reference from references in the **YamlGenerator** project.
7. Select visual studio Tools > Nuget package manager > Package Manager Console and run the below command : \
   `Install-Package YamlDotNet -Version 9.1.4`
8. Clean and build the project. 

## Steps (Build setup project and generate installation package)

1. After successfully building the main project, setup project can be built to generate installation package for
   latest project. Just right click on the setup project and click build.
2. After succesful build, the generated installer files can be found in the **bin\debug** folder. \
   <span style="color:red">**Note:**</span> All the files in the debug folder is necessary to install the plugin.
	
	


   

